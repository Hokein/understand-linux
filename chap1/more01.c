#include<stdio.h>
#define PAGELEN 24
#define LINELEN 512

void do_more(FILE *);
int see_more();

int main(int args, char *arg[])
{
	FILE *fp;

	if (args == 1)
		do_more(stdin);
	else
	{
		while (--args)
		{
			++arg;
			if ((fp = fopen(*arg, "r")) != NULL)
			{
				do_more(fp);
				close(fp);
			}
			else
				exit(1);
		}
	}
	return 0;
}


void do_more(FILE * fp)
{
	char line[LINELEN];
	int num_of_lines = 0;
	int reply;

	while (fgets(line, LINELEN, fp))
	{
		if (num_of_lines == PAGELEN)
		{
			reply = see_more();
			if (!reply)
				break;
			num_of_lines -= reply;
		}

		if (fputs(line, stdout) == EOF)
			exit(1);
		num_of_lines++;
	}
}

int see_more()
{
	int c;
	printf("\033[7m more? \033[m");

	while ((c = getchar()) != EOF)
	{
		switch (c)
		{
			case 'q':
				return 0;
			case ' ':
				return PAGELEN;
			case '\n':
				return 1;
		}
	}
}

