#include <stdio.h>
#include <utmp.h>
#include <fcntl.h>
#include <sys/types.h>
#include <time.h>
/*#define SHOWHOST 1*/

void show_info(struct utmp *utbufp)
{
	if (utbufp->ut_type != USER_PROCESS)
		return;
	printf("% -8.8s ", utbufp->ut_user);
	printf("% -8.8s ", utbufp->ut_line);
	//printf("%10ld ", utbufp->ut_tv.tv_sec);
	char *cp = ctime(&utbufp->ut_tv.tv_sec);
	printf("%12.12s", cp+4);

#ifdef SHOWHOST
	printf("(%s)", utbufp->ut_host);
#endif
	printf("\n");
}

int main()
{
	struct utmp* utbufp;

	if (utmp_open(UTMP_FILE) == -1)
	{
		perror(UTMP_FILE);
		exit(1);
	}

	while ((utbufp = utmp_next()) != (NULL))
		show_info(utbufp);
	utmp_close();
	return 0;

}
